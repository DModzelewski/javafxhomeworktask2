package main;

import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MainScene extends Application{
    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Group root = new Group();

        BorderPane borderPane = new BorderPane();
        HBox hbox = new HBox();
        ToggleButton button = new ToggleButton("Start/Stop");
        Pane pane = new Pane();
        Polygon star = new Polygon(300,200,271,260,205,269,252,315,241,381,300,350,359,381,348,315,395,269,329,260);
        star.setFill(Color.RED);
        pane.getChildren().add(star);

        borderPane.setLeft(hbox);
        borderPane.setCenter(pane);

        root.getChildren().add(borderPane);
        root.getChildren().add(hbox);
        root.getChildren().add(pane);
        root.getChildren().add(button);
        root.getChildren().add(star);

        button.setUserData(fullSequence(star));
        button.setOnAction(event -> {
            ToggleButton tb = (ToggleButton) event.getSource();
            Transition transite = (Transition) tb.getUserData();
            if (tb.isSelected()) {
                transite.play();
            } else {
                transite.pause();
            }

        });

        Scene scene = new Scene(root, 600, 600, Color.BLACK);
        stage.setScene(scene);
        stage.show();
    }

    private Transition moveUp(Node node){
        TranslateTransition tt = new TranslateTransition(Duration.millis(2000), node);
        double initialX = tt.getByX();
        double initialY = tt.getByY();
        tt.setFromX(initialX);
        tt.setFromY(initialY);
        tt.setToX(0);
        tt.setToY(-200);
        return tt;
    }

    private Transition moveRight(Node node){
        TranslateTransition tt = new TranslateTransition(Duration.millis(2000), node);
        tt.setFromX(0);
        tt.setFromY(-200);
        tt.setToX(200);
        tt.setToY(0);
        return tt;
    }

    private Transition goCenter(Node node){
        TranslateTransition tt = new TranslateTransition(Duration.millis(2000), node);
        tt.setFromX(-200);
        tt.setFromY(0);
        tt.setToX(0);
        tt.setToY(0);
        return tt;
    }

    private Transition changeColor(Shape node){
        FillTransition ft = new FillTransition(Duration.millis(2000), node);
        ft.setFromValue(Color.RED);
        ft.setToValue(Color.CORNFLOWERBLUE);
        return ft;
    }

    private Transition lastMove(Node node){
        ParallelTransition pt = new ParallelTransition(node);
        pt.getChildren().add(goCenter(node));
        pt.getChildren().add(changeColor((Shape)node));
        return pt;
    }

    private Transition rotate(Node node){
        RotateTransition rt = new RotateTransition(Duration.millis(2000), node);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        return rt;
    }

    private Transition moveRightRotate(Node node){
        ParallelTransition pt = new ParallelTransition(node);
        pt.getChildren().add(moveRight(node));
        pt.getChildren().add(rotate(node));
        return pt;
    }

    private Transition goDown(Node node){
        TranslateTransition tt = new TranslateTransition(Duration.millis(2000), node);
        tt.setFromX(200);
        tt.setFromY(0);
        tt.setToX(0);
        tt.setToY(220);
        return tt;
    }

    private Transition goLeft(Node node){
        TranslateTransition tt = new TranslateTransition(Duration.millis(2000), node);
        tt.setFromX(0);
        tt.setFromY(220);
        tt.setToX(-200);
        tt.setToY(0);
        return tt;
    }

    private Transition fullSequence(Node node){
        SequentialTransition st = new SequentialTransition();
        st.getChildren().addAll(moveUp(node),
                moveRightRotate(node),
                goDown(node),
                goLeft(node),
                lastMove(node));
        for (Animation a : st.getChildren()) {
            a.setCycleCount(1);
        }
        st.setCycleCount(Timeline.INDEFINITE);
        return st;
    }

}
